<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="<?='clave';  ?>">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>{{ $titulo }}</h4>
      </div>
      <div class="modal-footer">
          <div class="form-group ">
            <label class="pull-left">{{ $label }}</label>
            <input type="text" class="form-control" name="{{ $nombre }}">
          </div>
          <button type="submit" class="{{ $botonClass }}"> {{ $boton }}</button>
          <button data-dismiss="modal" class="btn btn-default"> Cerrar</i></button>
        
      </div>

    </div>
  </div>
</div>