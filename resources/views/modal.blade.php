<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="<?='modal'. $i;  ?>">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>{{ $titulo }}</h4>
      </div>
      <div class="modal-footer">
        
          <a href="{{ $href }}" class="{{ $botonClass }}"> {{ $boton }}</a>
          <button data-dismiss="modal" class="btn btn-default"> Cerrar</i></button>
        
      </div>

    </div>
  </div>
</div>