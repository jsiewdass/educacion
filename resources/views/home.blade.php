@extends('app')
@section('css')
 <link href="{{ asset('bower_components/pnotify/dist/pnotify.css') }}" rel="stylesheet">
@endsection

@section('content')


   <div class="row">
    <div class="col-md-12">
       <div class="panel panel-default">

    		<div class="panel-heading">
                <h4 class="box-title">
                	PIZARRA PRINCIPAL
                </h4>
            </div>
             <div class="panel-body">
        <div class="row">
          <!-- GRAFICO DE SEGUIMIENTOS SEMANAL -->
        <div class="col-md-9">
          <div class="chart">
            <div>
              <canvas id="seguimientos"></canvas>
              </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>1/S</h3>
              <p>Ventas / Mes</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">Mas Informacion <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>4</h3>
              <p>Tareas / Hoy</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">Mas Informacion <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col 
        <div class="col-lg-2 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>% de Conversion</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">EJEMPLO <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        -->
        <div class="col-md-9">
          <div class="chart">
            <div>
              <canvas id="ventas"></canvas>
              </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>3</h3>

              <p>Clientes Asociados</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">EJEMPLO <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('bower_components/pnotify/dist/pnotify.js') }}"></script>
   @include('prueba')
   
@endsection
