@extends('app-error')

@section('htmlheader_title')
    Error enviar correo
@endsection

@section('contentheader_title')
    Error al enviar los terminos de renovacion
@endsection

@section('$contentheader_description')
@endsection

@section('main-content-error')

<div class="error-page">
    <h2 class="headline text-red"> No se pude enviar el correo electrponico</h2>
    <div class="error-content">
        
        <p>
            Intente mas Tarde <a href='{{ url('/home') }}'>pagina principal</a>
        </p>
       
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection