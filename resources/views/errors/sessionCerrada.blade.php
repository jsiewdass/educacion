@extends('app-error')

@section('htmlheader_title')
    Sesión Cerrada
@endsection

@section('main-content-error')

<div class="error-page">
    <h2 class="headline text-red"> Sesión Cerrada</h2>
    <div class="error-content">
        <h3><i class="fa fa-danger text-red"></i> Su conexión ha finalizado</h3>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection
