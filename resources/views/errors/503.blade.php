@extends('app-error')

@section('htmlheader_title')
    Service unavailable
@endsection

@section('contentheader_title')
    503 Error Page
@endsection

@section('$contentheader_description')
@endsection

@section('main-content-error')

    <div class="error-page">
        <h2 class="headline text-red">530</h2>
        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Algo salio mal.</h3>
          <p>
              Trabajaremos para arreglarlo.
              vuelva a la  <a href='{{ url('/home') }}'>pagina principal</a>
          </p>
            <form class='search-form'>
                <div class='input-group'>
                    <input type="text" name="search" class='form-control' placeholder="Search"/>
                    <div class="input-group-btn">
                        <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i></button>
                    </div>
                </div><!-- /.input-group -->
            </form>
        </div>
    </div><!-- /.error-page -->
@endsection
