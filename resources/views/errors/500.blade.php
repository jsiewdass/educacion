@extends('app-error')

@section('htmlheader_title')
    Server error
@endsection

@section('contentheader_title')
    500 Error Page
@endsection

@section('$contentheader_description')
@endsection

@section('main-content-error')

    <div class="error-page">
        <h2 class="headline text-red">500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Oops! Algo salio mal.</h3>
            <p>
                Trabajaremos para arreglarlo.
                vuelva a la  <a href='{{ url('/home') }}'>pagina principal</a>
            </p>

        </div>
    </div><!-- /.error-page -->
@endsection
