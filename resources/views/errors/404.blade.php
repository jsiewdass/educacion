@extends('app-error')

@section('htmlheader_title')
    Pagina no encontrada
@endsection

@section('contentheader_title')
    Error 404 
@endsection

@section('$contentheader_description')
@endsection

@section('main-content-error')

<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Pagina no encontrada.</h3>
        <p>
            Parece que ha habido un error en la pagina que buscabas.
            vuelva a la <a href='{{ url('/home') }}'>pagina principal</a>
        </p>
        <!--<form class='search-form'>
            <div class='input-group'>
                <input type="text" name="search" class='form-control' placeholder="Buscar"/>
                <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                </div>
            </div><!-- /.input-group -->
        <!--</form>-->
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection
