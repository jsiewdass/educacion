<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Font Awesome -->

    {{--REVISAR ESTAS DOS LINEAS PORQUE CAMBIAN EL CCS--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">

    <!-- Ionicons -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">--}}
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">

    <!-- Theme style -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css">--}}
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/_all-skins.min.css">--}}
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">

   
    @yield('css')
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    

    @yield('content')

    

  </div>
  <!-- /.login-box-body -->
</div>

            {{--REVISAR LOS CODIGOS DEL CDN A LAS LIBRERIAS AQUI--}}

    <!-- jQuery 3.1.1 -->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
   
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('plugins/iCheck/icheck.min.js')}}" type="text/javascript"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@yield('scripts')
</body>
</html>
