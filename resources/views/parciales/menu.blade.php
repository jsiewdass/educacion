<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ asset('images/user.png') }}" class="img-circle">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name .' '.Auth::user()->apellido }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div><br><br><br>
      </div>
      <!-- search form 

      <div class="sidebar-form">
        <div class="input-group">
          <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Buscar ticket...">
          <input type="hidden" id="rutaBuscar" value="{{ url('buscarTicket') }}" >
          <span class="input-group-btn">
            <a type="button" name="search"  class="btn btn-flat" id="modalClick"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
      -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
            <li><a type="button" href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @permission('cliente-list')
           <li class="treeview">
              <a href="#"><i class="fa fa-user-circle-o"></i> <span>Clientes</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('cliente/create') }}"><i class="fa fa-plus"></i> <span>Crear</span></a></li>
                  <li><a href="{{ url('cliente') }}"><i class="fa fa-list"></i> <span>Ver</span></a></li>
              </ul>
            </li>
            @endpermission
            @permission('seguimiento-list')
           <li class="treeview">
              <a href="#"><i class="fa fa-briefcase"></i> <span>Seguimiento</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('seguimiento/create') }}"><i class="fa fa-plus"></i> <span>Crear</span></a></li>
                  <li><a href="{{ url('seguimiento') }}"><i class="fa fa-list"></i> <span>Ver</span></a></li>
              </ul>
            </li>
            @endpermission
            @permission('tarea-list')
            <li><a type="button" href="{{ url('tarea') }}"><i class="fa fa-calendar"></i> <span>Tareas</span></a></li>
            @endpermission
            @permission('venta-list')
           <li class="treeview">
              <a href="#"><i class="fa fa-shopping-cart"></i> <span>Ventas o Pagos</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('venta/create') }}"><i class="fa fa-plus"></i> <span>Crear</span></a></li>
                  <li><a href="{{ url('venta') }}"><i class="fa fa-list"></i> <span>Ver Ventas</span></a></li>
                 
              </ul>
            </li>
            @endpermission
            @permission('planificacion-list')
           <li class="treeview">
              <a href="#"><i class="fa fa-tasks"></i> <span>Planificación</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('planificacion/create') }}"><i class="fa fa-plus"></i> <span>Crear</span></a></li>
                  <li><a href="{{ url('planificacion') }}"><i class="fa fa-list"></i> <span>Ver</span></a></li>
                  <li><a href="{{ url('planificacion/show') }}"><i class="fa fa-calendar"></i> <span>Calendario</span></a></li>
              </ul>
            </li>
            @endpermission
            <!--
            <li class="treeview">
              <a href="#"><i class="fa fa-line-chart"></i> <span>Reportes </span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('reporte/cliente') }}"><i class="fa fa-user"></i> <span>Clientes</span></a></li>
                  <li><a href="{{ url('reporte/asunto') }}"><i class="fa fa-user"></i> <span>Asuntos</span></a></li>
                  <li><a href="{{ url('reporte/sociedad') }}"><i class="fa fa-user"></i> <span>Sociedades</span></a></li>
                  <li><a href="{{ url('reporte/fundacion') }}"><i class="fa fa-user"></i> <span>Fundaciones</span></a></li>
                  <li><a href="{{ url('reporte/bien') }}"><i class="fa fa-user"></i> <span>Bienes Raices</span></a></li>
                  <li><a href="{{ url('reporte/referidor') }}"><i class="fa fa-user"></i> <span>Referidor</span></a></li>
              </ul>
            </li> 
            -->
            <!--/////////////// -->

            @permission('definicion-list')  
            <li class="treeview">
              <a href="#"><i class="fa fa-building-o"></i> <span>Definiciones </span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{ url('tipo') }}"><i class="fa fa-list"></i> <span>Tipo de seguimientos</span></a></li>
                  <li><a href="{{ url('medio') }}"><i class="fa fa-list"></i> <span>Medio de seguimientos</span></a></li>
                  <li><a href="{{ url('especialidad') }}"><i class="fa fa-list"></i> <span>Especialidad Cliente</span></a></li>
                  <li><a href="{{ url('aula') }}"><i class="fa fa-list"></i> <span>Aulas</span></a></li>
                  <li><a href="{{ url('estatus') }}"><i class="fa fa-list"></i> <span>Estatus de clientes</span></a></li>
                  <li><a href="{{ url('actividad') }}"><i class="fa fa-list"></i> <span>Tipo de actividades</span></a></li>
              </ul>
            </li> 
            @endpermission


            <!--//////////////  -->    
          @permission('admin-ver')  
            <li class="treeview">
              <a href="#"><i class="fa fa-cogs"></i> <span>Administración</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @permission('user-list') 
                  <li><a href="{{ url('users') }}"><i class="fa fa-user"></i> <span>Usuario</span></a></li>
                @endpermission
                @permission('role-list')
                  <li><a href="{{ url('roles') }}"><i class="fa fa-address-card"></i> <span>Roles</span></a></li>
                @endpermission
                <li><a href="{{ url('curso') }}"><i class="fa fa-list"></i> <span>Cursos</span></a></li>
              </ul>
            </li>
          @endpermission
            <!--
            <li><a href="#"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
      </ul>
      
    </section>

    <!-- /.sidebar -->