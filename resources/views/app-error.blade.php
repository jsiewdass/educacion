<!DOCTYPE html>
<html lang="en">
  <head>
  	@include('parciales.header')
    @yield('css')
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
   
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
    </section>
  </aside>
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <!-- page content -->
            <div class="right_col" role="main">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  
                      <div class="col-md-12">
                        <!-- <h3>Nombre de Sistemas <small>Titulo del Sistema</small></h3> -->
                        
                        @yield('main-content-error')
                        
                      </div>
                    
                </div>
              </div>
            </div>  
          <!-- /page content -->
    </section>
    <!-- /.content -->
  </div>
           



          @include('parciales.footer')
  	  </div><!-- /main_container -->
    </div><!-- /container body -->
  	
  	@include('parciales.script')
  	@yield('scripts')
  </body>
</html>