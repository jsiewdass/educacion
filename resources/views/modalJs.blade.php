
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="myModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="modal-titulo"></h4>
      </div>
      <div class="modal-footer">
        
          <a class="btn btn-danger" id="eliminarRegistro"> Eliminar</a>
          <button data-dismiss="modal" class="btn btn-default"> Cerrar</i></button>
        
      </div>

    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="cargando">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="modal-titulo"></h4>
      </div>
      <div class="modal-footer">
        
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0"
                 aria-valuemin="0" aria-valuemax="100" style="">
            </div>
          </div>
        
      </div>
      

    </div>
  </div>
</div>