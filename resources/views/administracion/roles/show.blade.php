@extends('app')
 @section('panel')
<div class="container">
  <h3>Ver Perfil 
  <small class="pull-right">
     <a class="btn btn-primary" href="{{ route('roles.index') }}">
     <i class="fa fa-arrow-circle-left"></i>Volver</a>
  </small>
</h3>
</div>
@endsection

@section('content')
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $role->display_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                {{ $role->description }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Permisos:</strong>
                @if(!empty($rolePermissions))
					@foreach($rolePermissions as $v)
						<label class="label label-success">{{ $v->display_name }}</label>
					@endforeach
				@endif
            </div>
        </div>
	</div>
@endsection