@extends('app')
@section('css')
 <link href="{{ asset('bower_components/pnotify/dist/pnotify.css') }}" rel="stylesheet">
@endsection
@section('content')
	
	
	<div class="panel panel-default">
	<div class='notifications top-right'></div>
    <div class="panel-heading">
        <h3>Perfiles
		  <small class="pull-right">
		     <a class="btn btn-primary" href="{{ route('roles.create') }}">Crear Perfil</a>
		  </small>
		</h3>
    </div>

    <div class="panel-body">
        <table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th width="280px">Acción</th>
			</tr>
		</thead>
	@foreach ($roles as $key => $role)
	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $role->display_name }}</td>
		<td>{{ $role->description }}</td>
		<td>
			<div class="btn-group" role="group" aria-label="...">

				<a class="btn btn-info btn-sm" href="{{ route('roles.show',$role->id) }}">
					<i class="fa fa-address-card-o"></i>
				</a>
				@permission('role-edit')
				<a class="btn btn-primary btn-sm" href="{{ route('roles.edit',$role->id) }}">
					<i class="fa fa-pencil"></i>
				</a>
				@endpermission
				@permission('role-delete')
				<a type="button" href="" name="resumen" title="Inactivar" class="btn btn-danger btn-sm" 
		            data-toggle="modal" data-target="<?='#modal'. $i;  ?>">
		              <i class="fa fa-trash"></i>
		            </a>
	        	@endpermission
			</div>
		</td>
	</tr>
	@php
        ////////////////////////////////////////////////////////////////////////
        ///////////////// DATOS DEL MODAL PARA INACTIVAR////////////////////////
        ////////////////////////////////////////////////////////////////////////
        $titulo = '¿Desea eliminar el Perfil ' . $role->name.' ?'; 
        $href = url('roles/destroy/'.$role->id);
        $boton  = 'Eliminar';
        $botonClass = 'btn btn-danger';
        ///////////////////////////////////////////////////////////////////////
     @endphp 
        @include('modal')
	@endforeach
	</table>
	{!! $roles->render() !!}
        
    </div>

</div>
	
@endsection

@section('scripts')
   
    <script src="{{ asset('bower_components/pnotify/dist/pnotify.js') }}"></script>
   @include('prueba')
@endsection