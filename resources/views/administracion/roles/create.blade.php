@extends('app')

@section('content')
	<div class="panel panel-default">

    <div class="panel-heading">
          <h3>Crear nuevo perfil
              <small class="pull-right">
                 <a class="btn btn-primary" href="{{ route('roles.index') }}">
     <i class="fa fa-arrow-circle-left"></i>Volver</a>
              </small>
            </h3>
    </div>

    <div class="panel-body">
	{!! Form::open(array('route' => 'roles.store','method'=>'POST' , 'id'=> 'formRole')) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
            </div>
        </div>
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre para mostrar:</strong>
                {!! Form::text('display_name', null, array('placeholder' => 'Nombre para mostrar','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control','style'=>'height:100px')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Permisos:</strong>
                <br/>
                <div class="form-goup">
                	@php
                        $permiso = '';
                        $bandera = '';
                        $contador = 0;
                    @endphp
                @foreach($permission->sortBy('name') as $value)
                    @php

                        $permiso = explode('-', $value->name);
                    @endphp
                            @if($bandera == '')
                            @php
                                $bandera = $permiso[0];
                                //echo $bandera;
                            @endphp
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ strtoupper($permiso[0]) }}
                                    @php $contador++; @endphp</div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="icheckbox">
                                                <label>
                                                  {{ 
                                                    Form::checkbox('permission[]', $value->id, false, array('class' => 'name styled1')) }}
                                                    {{ $value->display_name }}
                                                  
                                                </label>                                          
                                            </div>
                                        </div>
                            @elseif($bandera == $permiso[0])
                            @php
                                //echo $bandera. '='.$permiso[0];
                                $permiso = explode('-', $value->name);
                                //$bandera = $permiso[0];
                            @endphp
                                    <div class="form-group">
                                        <div class="icheckbox">
                                            <label>
                                              {{ 
                                                Form::checkbox('permission[]', $value->id, false, array('class' => 'name styled1')) }}
                                                {{ $value->display_name }}
                                            </label>
                                        </div>
                                    </div>
                            @elseif($bandera != $permiso[0] )          
                            @php
                                $permiso = explode('-', $value->name);
                                $bandera = $permiso[0];
                                //echo $bandera;
                            @endphp
                                    </div>
                                </div>
                            </div>
                            @if($contador == 4)
                                </div>
                                <div class="row">
                            @endif
                            <div class="col-md-3 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ strtoupper($permiso[0]) }}
                                        @php $contador++; @endphp
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="icheckbox">
                                                <label>
                                                  {{ 
                                                    Form::checkbox('permission[]', $value->id, false, array('class' => 'name styled1')) }}
                                                    {{ $value->display_name }}
                                                  
                                                </label>                                            
                                            </div>
                                        </div>
                            @endif
                          
                @endforeach
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </div>
	</div>
	{!! Form::close() !!}
    </div>

</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\RoleRequest', '#formRole') !!}
    <script>
        $('.styled1').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        //radioClass: 'iradio_minimal',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
    </script>
@endsection