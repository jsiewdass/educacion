@extends('app')
@section('css')
 <link href="{{ asset('bower_components/pnotify/dist/pnotify.css') }}" rel="stylesheet">
@endsection

@section('content')
	

	<div class="panel panel-default">

    <div class="panel-heading">
          <h3>Usuarios
			  <small class="pull-right">
					<button type='button' name='buscar' id='buscar' data-toggle='modal' data-target='#myModal'  class='delete-modal btn btn-info' ><i class='fa fa-search'></i></button>
			     	<a class="btn btn-primary" href="{{ route('users.create') }}"> Crear Usuario</a>
			  </small>
			</h3>
    </div>

    <div class="panel-body">
    <div class='notifications top-right'></div>
    <div class="table-responsive">
    	<table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre y Apellido</th>
				<th>Documento de identidad</th>
				<th>Correo Electronico</th>
				
				<th>Perfiles</th>
				
				<th width="150px">Acción</th>
			</tr>
		</thead>

	@foreach ($data as $key => $user)
	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $user->name .' '. $user->apellido }}</td>
		<td>{{ $user->username }}</td>
		<td>{{ $user->email }}</td>
		
		<td>
			@if(!empty($user->roles))
				@foreach($user->roles as $v)
					<label class="label label-success">{{ $v->display_name }}</label>
				@endforeach
			@endif
		</td>
		
		
		<td >
			<div class="btn-group hidden-xs" role="group" aria-label="...">
					<a class="btn btn-sm btn-info" href="{{ route('users.show',Crypt::encrypt($user->id)) }}" title="Ver usuario">
					<i class="fa fa-address-card-o"></i>
					</a>
				@permission('user-edit')
					<a class="btn btn-sm btn-primary" href="{{ route('users.edit',Crypt::encrypt($user->id)) }}" title="Editar usuario">
						<i class="fa fa-pencil"></i>
					</a>
				@endpermission
				@permission('user-delete')
					<a type="button" href="" name="resumen" title="Inactivar" class="btn btn-sm btn-danger" 
			            data-toggle="modal" data-target="<?='#modal'. $i;  ?>">
			              <i class="fa fa-trash"></i>
			            </a>
			    @endpermission
			</div>	
			            <?php
			            ////////////////////////////////////////////////////////////////////////
			            ///////////////// DATOS DEL MODAL PARA INACTIVAR////////////////////////
			            ////////////////////////////////////////////////////////////////////////
			            $titulo = '¿Desea eliminar el usuario ' . $user->name.' '.$user->apellido .' ?'; 
			            $href = url('users/destroy/'.$user->id);
			            $boton  = 'Eliminar';
			            $botonClass = 'btn btn-danger';
			            ///////////////////////////////////////////////////////////////////////
			            ?> 
			             
	        	
	        
	        @include('modal')
		</td>
	</tr>
	@endforeach
	</table>
    </div>
		@include('administracion.users.filter')
	{!!$data->appends(Request::only(['name', 'apellido', 'role', 'email', 'cedula']))->render()!!}
    </div>

</div>
	
@endsection
@section('scripts')
   
    <script src="{{ asset('bower_components/pnotify/dist/pnotify.js') }}"></script>
   @include('prueba')
@endsection