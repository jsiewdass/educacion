{!! Form::model(Request::all(), ['route' => 'users.index', 'method' => 'GET']) !!}

  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3>Filtro de Búsqueda</h3>
        </div>
        <div class="modal-body">
            
                <div class="form-group">
                 {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
                </div><!-- /input-group -->
                <div class="form-group">
                  {!! Form::text('apellido', null, ['class' => 'form-control', 'placeholder' => 'Apellido']) !!}
                </div><!-- /input-group -->
                <div class="form-group">
                 {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Nombre de usuario']) !!}
                </div><!-- /input-group -->
                <div class="form-group">
                  {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                </div><!-- /input-group -->
                <div class="form-group">
                  {!! Form::select('role',$role, null, ['class' => 'form-control', 'placeholder' => 'Perfil']) !!}
              </div><!-- /input-group -->
              
              
             
            
        </div>
        <input type="hidden" id="btn">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="buscar btn btn-info" >Buscar</button>
        </div>
      </div>
    </div>
  </div>

   

{!! Form::close() !!}