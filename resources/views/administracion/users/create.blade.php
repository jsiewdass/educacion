@extends('app')

@section('css')
    
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
          <h3>Crear nuevo usuario
              <small class="pull-right">
                 <a class="btn btn-primary" href="{{ route('users.index') }}">
     <i class="fa fa-arrow-circle-left"></i>Volver</a>
              </small>
            </h3>
    </div>

    <div class="panel-body">
   
	{!! Form::open(array('route' => 'users.store','method'=>'POST', 'id'=> 'formUser')) !!}
	<div class="container">
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Nombre:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Apellido:</strong>
                {!! Form::text('apellido', null, array('placeholder' => 'Apellido','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Documento identidad:</strong>
                {!! Form::text('cedula', null, array('placeholder' => 'Documento identidad','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Correo electronico:</strong>
                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Contraseña:</strong>
                {!! Form::password('password', array('placeholder' => 'Contraseña','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Confirmar Contraseña:</strong>
                {!! Form::password('confirm-password', array('placeholder' => 'Confirmar Contraseña','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Perfil:</strong>
                {!! Form::select('roles[]', $roles,[], array('class' => 'form-control multiple','multiple', 'placeholder' => 'Seleccione el Perfil')) !!}
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Registrar</button>

        </div>
    </div>
    {!! Form::close() !!}  
    </div>

</div>
@endsection
@section('scripts')
    <script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
     <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\UserRequest', '#formUser') !!}
    <script type="text/javascript">
            $('.multiple').select2();
    </script>
@endsection