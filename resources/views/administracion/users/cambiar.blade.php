@extends('app')


@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
          <h3>Editar Contraseña
              <small class="pull-right">
                 <a class="btn btn-primary" href="{{ url()->previous() }}">
     <i class="fa fa-arrow-circle-left"></i>Volver</a>
              </small>
            </h3>
    </div>

    <div class="panel-body">

     {!! Form::open(['method' => 'PATCH','route' => 'users.cambiar', 'id' => 'formPassword']) !!}
     
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Actual contraseña:</strong>
                {!! Form::password('current-password', array('placeholder' => 'Ingrese la nueva contraseña','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Nueva contraseña:</strong>
                {!! Form::password('password', array('placeholder' => 'Ingrese la nueva contraseña','class' => 'form-control')) !!}
            </div>
        </div>
       
    </div>
    <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Confirmar contraseña:</strong>
                {!! Form::password('password_confirmation', array('placeholder' => 'Ingrese confirmar contraseña','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row"> 
       
        <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
                <button type="submit" class="btn-block btn btn-primary">Editar</button>
        </div>
    </div>
    {{ csrf_field() }}
    {!! Form::close() !!}   
   </div>

</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('vendors/jsvalidation/js/jsvalidation.js') }}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\PasswordRequest', '#formPassword') !!}
   
@endsection
