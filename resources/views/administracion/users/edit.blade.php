@extends('app')


@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
          <h3>Editar usuario
              <small class="pull-right">
                 <a class="btn btn-primary" href="{{ route('users.index') }}">
     <i class="fa fa-arrow-circle-left"></i>Volver</a>
              </small>
            </h3>
    </div>

    <div class="panel-body">

     {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'id' => 'formUser']) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Nombre:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Apellido:</strong>
                {!! Form::text('apellido', null, array('placeholder' => 'Apellido','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Documento identidad:</strong>
                {!! Form::text('cedula', null, array('placeholder' => 'Documento identidad','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Correo electronico:</strong>
                {!! Form::text('email', null, array('placeholder' => 'Correo electronico','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Perfil:</strong>
                {!! Form::select('roles[]', $roles, $userRole, array('class' => 'form-control multiple','multiple')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Nueva contraseña:</strong>
                {!! Form::password('passwords', array('placeholder' => 'Ingrese la nueva contraseña','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Confirmar contraseña:</strong>
                {!! Form::password('confirm-password', array('placeholder' => 'Ingrese confirmar contraseña','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row"> 
       
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary ">Editar</button>
        </div>
    </div>
    {!! Form::close() !!}   
   </div>

</div>
@endsection
@section('scripts')
    <script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\EditUserRequest', '#formUser') !!}
    <script type="text/javascript">
            $('.multiple').select2();
    </script>
@endsection
@section('css')
    
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    

@endsection