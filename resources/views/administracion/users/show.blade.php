@extends('app')
 
@section('content')

<div class="row">
	<div class="col-md-4">
		<!-- PORTLET MAIN -->
<div class="portlet light profile-sidebar-portlet">
	<!-- SIDEBAR USERPIC -->
	<div class="profile-userpic">
		<img src="{{ url($img) }}" class="img-responsive" alt="">
	</div>
	<!-- END SIDEBAR USERPIC -->
	<!-- SIDEBAR USER TITLE -->
	<div class="profile-usertitle">
		<div class="profile-usertitle-name">
			 {{ $user->name.' '.$user->apellido }}
		</div>
		<div class="profile-usertitle-job">
			 @if(!empty($user->roles))
				@foreach($user->roles as $v)
					{{ $v->display_name.' ' }}
				@endforeach
			@endif
		</div>
	</div>
	
	<div class="profile-usermenu">
		<ul class="nav">
			<li class="active">
				<a >
				<i class="icon-check"></i>
				{{ $user->email }}</a>
			</li>
			
			
		</ul>
	</div>
	<!-- END MENU -->
</div>
<!-- END PORTLET MAIN -->

		

	</div>
</div><!-- END ROW -->
	


@endsection