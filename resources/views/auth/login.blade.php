@extends('layouts.app')

@section('content')

<p class="login-box-msg">Inicio de Sesión</p>          
<form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }} has-feedback">
        
        <div class="form-group has-feedback">
            <input id="login" type="text" class="form-control" name="login" placeholder="Ingrese el Correo o nombre de usuario" autofocus>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('login'))
                <span class="help-block">
                    <strong>{{ $errors->first('login') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        

        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Ingrese Contraseña" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="row">
    <div class="col-xs-8">
      &nbsp;
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
    </div>
    <!-- /.col -->
  </div>

  <!--<a href="{{ url('password/reset') }}">Olvidaste tu clave</a>-->
</form>
                
@endsection
