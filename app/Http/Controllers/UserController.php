<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use DB;
use Hash;
use Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\UserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\PasswordRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$u = User::roles()->where('roles.name', 'admin')->get();
        //dd($u);
        $user_id = Auth::user()->id;
        $ver = [];
        switch ($user_id) {
            case 1: case 2: case 3: case 4:
                $ver = [];
                break;
            
            default:
               $ver = [1,2,3,4];
                break;
        }
        $data = User::name($request->get('name'))
                      ->apellido($request->get('apellido'))
                      ->username($request->get('username'))
                      ->role($request->get('role'))
                      ->email($request->get('email'))
                      ->orderBy('id','DESC')
                      ->whereNotIn('id', $ver)
                      ->paginate(10);
        //dd($data);
        $role = Role::pluck('name', 'name');
        return view('administracion.users.index',compact('data', 'role'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','id');
        
        return view('administracion.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {   //dd($request);
        /*$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',
           
        ]);*/

        $input = $request->all();
        //dd($input);
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
       

        return redirect()->route('users.index')
                        ->with('success','Usuario creado Satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $user = User::find($id);
        
        $cedula= $user->cedula;
            $primerPunto = substr($cedula, -3, 3);
            $segundoPunto = substr($cedula, -6, 3);
            
            if (strlen($cedula)==8) {
              $resto = substr($cedula, 0, 2);
            }else{
              $resto = substr($cedula, 0, 1);
            }
            $foto = $resto.".".$segundoPunto.".".$primerPunto;
            //dd($resto);
            $file = 'http://fotos.mp.gob.ve/'.$foto.'.jpg';
            $file_headers = @get_headers($file);



            if ($cedula == '18491633') {

                $img = asset('/img/'.$foto.'.png'); 

            }
            else{
                $img = asset('/img/user.png');
            }
            //$ruta = 'http://fotos.mp.gob.ve/'.$foto.'.jpg';

            
        return view('administracion.users.show',compact('user', 'img', 'ruta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $id = Crypt::decrypt($id);
        $user = User::find($id);
        $roles = Role::pluck('display_name','id');
        
        $userRole = $user->roles->pluck('id','id')->toArray();
        
        
        //dd($userEstado);

        return view('administracion.users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'roles' => 'required',
            'password' => 'same:confirm-password',
           

            
        ]);

        $input = $request->all();
        if (!empty($input['passwords'])) {
            $input['password'] = Hash::make($input['passwords']);
        }
        
        $user = User::find($id);
        $user->update($input);
        DB::table('role_user')->where('user_id',$id)->delete();
        

        
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        

        return redirect()->route('users.index')
                        ->with('success','Usuario editado Satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','Usuario eliminado Satisfactoriamente');
    }

    public function cambiarForm(){

        return view('administracion.users.cambiar');
    }

    public function cambiar(PasswordRequest $request){

        
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('home')->with('success','Su contraseña se modifico Satisfactoriamente');
        //return redirect()->back()->with('success','Balance agregado Satisfactoriamente');;
    }
}
