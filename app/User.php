<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'apellido', 'telefono', 'email', 'password',  'dni'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setApellidoAttribute($value)
    {
        $this->attributes['apellido'] = strtoupper($value);
    }

    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = strtoupper($value);
    }
    //'App\User', 'foreign_key', 'other_key'
    
    public function scopeName($query, $name){
      //dd('scope' . $sede);

      if ($name != '') {
        $name = strtoupper($name);
        $query->where("name", "like", "%$name%");
      }

    }
    public function scopeApellido($query, $apellido){
      //dd('scope' . $sede);

      if ($apellido != '') {
        $apellido = strtoupper($apellido);
        $query->where("apellido", "like", "%$apellido%");
      }

    }

    public function scopeUsername($query, $username){
      //dd('scope' . $sede);

      if ($username != '') {
        $username = strtoupper($username);
        $query->where("username", "like", "%$username%");
      }

    }
    public function scopeEmail($query, $email){
      //dd('scope' . $sede);

      if ($email != '') {
        $email = strtoupper($email);
        $query->where("email", "like", "%$email%");
      }

    }
    public function scopeRole($query, $role){
      //dd('scope' . $sede);

      return $query->whereHas(
        'roles', function($query) use ($role){
            $query->where('name', 'like', "%$role%");
        }
      );

    }
  }
