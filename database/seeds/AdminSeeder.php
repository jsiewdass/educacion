<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\UserRol;
use App\PermisoRol;
use App\Permission;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $userjs = User::create([
            'username' => 'jsiewdass',
            'name' =>'Jonas',
            'apellido' => 'Siewdass',
            'telefono' => '04169455470',
            'email' => 'jsiewdass@gmail.com',
            'password' => bcrypt('abc.1234'),
            'remember_token' => bcrypt('abc.1234'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
                
        //////////////


        $rol = Role::create([
        	'name' => 'admin',
        	'display_name' => 'Administrador',
        	'description' => 'Administrador(es) del sistema',
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),


        ]);
        $estandar = Role::create([
            'name' => 'estandar',
            'display_name' => 'Vendedora',
            'description' => 'El usuario Vendedora No crea usuarios',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
       
        //$usermg->attachRole($estandar);
        $userjs->attachRole($rol);

            $rl = Permission::create([
                'name' => 'role-list',
                'display_name' => 'Listar Perfil',
                'description' => 'Solo puede ver el Perfil'
            ]);
             $rol->attachPermission($rl);
            $rc = Permission::create([
                'name' => 'role-create',
                'display_name' => 'Crear Perfil',
                'description' => 'Create Nuevo Perfil'
            ]);
             $rol->attachPermission($rc);
            $re = Permission::create([
                'name' => 'role-edit',
                'display_name' => 'Editar Perfil',
                'description' => 'Editar Perfil'
            ]);
             $rol->attachPermission($re);
            $rd = Permission::create([
                'name' => 'role-delete',
                'display_name' => 'Eliminar Perfil',
                'description' => 'Eliminar Perfil'
            ]);
             $rol->attachPermission($rd);
            $ul = Permission::create([
                'name' => 'user-list',
                'display_name' => 'Listar Usuario',
                'description' => 'Solo puede ver el Usuario'
            ]);
             $rol->attachPermission($ul);
            $uc = Permission::create([
                'name' => 'user-create',
                'display_name' => 'Crear Usuario',
                'description' => 'Create Nuevo Usuario'
            ]);
             $rol->attachPermission($uc);
            $ue = Permission::create([
                'name' => 'user-edit',
                'display_name' => 'Editar Usuario',
                'description' => 'Editar Usuario'
            ]);
             $rol->attachPermission($ue);
            $ud = Permission::create([
                'name' => 'user-delete',
                'display_name' => 'Eliminar Usuario',
                'description' => 'Eliminar Usuario'
            ]);
            $rol->attachPermission($ud);

            $av = Permission::create([
                'name' => 'admin-ver',
                'display_name' => 'Ver administración',
                'description' => 'Ver el modulo de administración'
            ]);
            //$local->attachPermission($av);
            $rol->attachPermission($av);

            
    }
}
